import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { FavoritIcons, JelajahiIcons, PetaTokoIcons, RiwayatIcons } from './src/icons';
import PesananIcons from './src/icons/PesananIcons';
import BlankView from './src/views/blank_view';
import Home from './src/views/home';

const App = () => {
  const BottomTab = createBottomTabNavigator();

  return (
    <>
      <NavigationContainer>
        <BottomTab.Navigator>
          <BottomTab.Screen name="Jelajahi" component={Home}
            options={{
              title: 'Jelajahi',
              tabBarIcon: ({ size, focused, color }) => {
                return (
                  <JelajahiIcons color={color} />
                );
              },
            }} />


          <BottomTab.Screen name="Peta Toko" component={BlankView} options={{
            title: 'Peta Toko',
            tabBarIcon: ({ size, focused, color }) => {
              return (
                <PetaTokoIcons color={color} />
              );
            },
          }} />

          <BottomTab.Screen name="Favorit" component={BlankView} options={{
            title: 'Favorit',
            tabBarIcon: ({ size, focused, color }) => {
              return (
                <FavoritIcons color={color} />
              );
            },
          }} />

          <BottomTab.Screen name="Pesanan" component={BlankView} options={{
            title: 'Pesanan',
            tabBarIcon: ({ size, focused, color }) => {
              return (
                <PesananIcons color={color} />
              );
            },
          }} />

          <BottomTab.Screen name="Riwayat" component={BlankView} options={{
            title: 'Riwayat',
            tabBarIcon: ({ size, focused, color }) => {
              return (
                <RiwayatIcons color={color} />
              );
            },
          }} />

        </BottomTab.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
