# How to run program
- Install dependencies, run command `npm install`
- Check adb connection devices by run command `adb devices`
- Connect computer localhost to phone, run command `adb reverse tcp:5000 tcp:5000`
- Compile apk-debug, run command `npx react-native run-android`
- APK installed

### Disclaimer, application built and tested on Android Emulator Google Pixel 2, Android OS 10