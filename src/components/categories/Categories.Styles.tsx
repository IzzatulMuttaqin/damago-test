import { StyleSheet } from "react-native";

export const categoriesStyles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        height: 250,
        width: 380,
        borderRadius: 15,
    },
    textStyle: {
        fontWeight: 'bold',
        fontSize: 22,
    },
    content: {
        width: 95,
        paddingBottom: 15,
        alignItems: 'center'
    },
    smallContainer: {
        backgroundColor: 'white',
        height: 150,
        width: 380,
        borderRadius: 15,
    },
    header: {
        flexDirection: "row",
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 15
    },
    contentContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});