import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { CancelIcons } from '../../icons';
import ChartIcons from '../../icons/ChartIcons';
import IconNames from '../icons_and_name';
import { categoriesStyles } from './Categories.Styles';
import { ICategories } from './interface';

const Categories = (props: ICategories) => {
    return (
        <>
            {!props.state
                ?
                <View style={categoriesStyles.smallContainer}>
                    <View style={categoriesStyles.header}>
                        <Text style={categoriesStyles.textStyle}>Kategori</Text>
                    </View>
                    <View style={categoriesStyles.contentContainer}>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Restoran" image_source={require('../../images/Restaurant.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Cafe" image_source={require('../../images/Cafe.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Hotel" image_source={require('../../images/hotel.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames onPress={props.onChange} icon_name="Lainnya" image_source={require('../../images/lain.png')} />
                        </View>
                    </View>
                </View>
                :
                <View style={categoriesStyles.container}>
                    <View style={{ flexDirection: "row", justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 15 }}>
                        <Text style={categoriesStyles.textStyle}>Lainnya</Text>
                        <TouchableOpacity onPress={props.onChange}>
                            <CancelIcons />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Restoran" image_source={require('../../images/Restaurant.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Cafe" image_source={require('../../images/Cafe.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Hotel" image_source={require('../../images/hotel.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Grosir" image_source={require('../../images/Grocery.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Toko Roti" image_source={require('../../images/Bakery.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Kaki Lima" image_source={require('../../images/streetfood.png')} />
                        </View>
                        <View style={categoriesStyles.content}>
                            <IconNames icon_name="Perikanan" image_source={require('../../images/perikanan.png')} />
                        </View>
                    </View>
                </View>
            }
        </>
    );
};

export default Categories;
