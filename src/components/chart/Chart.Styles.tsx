import { StyleSheet } from "react-native";

export const chartStyles = StyleSheet.create({
    container: {
        backgroundColor: '#00A3E0',
        height: 75,
        width: 75,
        position: "absolute",
        borderRadius: 37.5,
        bottom: 20,
        right: 10,
        paddingTop: 15,
    },
    chartPosition: {
        alignSelf: 'center',
        marginRight: 5,
    },
    textStyle: {
        fontSize: 16,
        color: '#FFFFFF',
        marginTop: 2.5,
        alignSelf: "center",
    }
});