import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import ChartIcons from '../../icons/ChartIcons';
import { chartStyles } from './Chart.Styles';

const Chart = () => {
    return (
        <>
            <TouchableOpacity style={chartStyles.container}>
                <View style={chartStyles.chartPosition}>
                    <ChartIcons />
                </View>
                <View>
                    <Text style={chartStyles.textStyle}>
                        3
                    </Text>
                </View>
            </TouchableOpacity>
        </>
    );
};

export default Chart;
