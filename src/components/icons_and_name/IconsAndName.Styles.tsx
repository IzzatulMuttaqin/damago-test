import { StyleSheet } from "react-native";

export const iconAndNameStyles = StyleSheet.create({
    container: {
        alignItems: "center",
    },
    textStyle: {
        paddingTop: 5,
    }
});