import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { iconAndNameStyles } from './IconsAndName.Styles';
import { IIconsName } from './interface';

const IconNames = (props: IIconsName) => {
    const toggle = () => {
        if (props.onPress && typeof props.onPress === 'function') props.onPress();
    }
    return (
        <>
            <TouchableOpacity onPress={toggle} style={iconAndNameStyles.container}>
                <Image source={props.image_source} />
                <View>
                    <Text style={iconAndNameStyles.textStyle}>
                        {props.icon_name}
                    </Text>
                </View>
            </TouchableOpacity>
        </>
    );
};

export default IconNames;
