export interface IIconsName {
    image_source: any;
    icon_name: string;
    onPress?: () => any;
}