import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: 'grey',
        height: 100,
        width: 300,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    upperContainer: {
        backgroundColor: 'white',
        height: 45,
        width: 45,
        alignSelf: 'center',
        borderBottomLeftRadius: 22.5,
        borderBottomRightRadius: 22.5,
    },
    pictStyle: {
        height: 35,
        width: 35,
        backgroundColor: 'grey',
        borderRadius: 17.5,
        alignSelf: 'center',
        marginTop: 5,
    },
    containerBottom: {
        height: 75,
        width: 300,
        borderBottomRightRadius: 10,
        borderBottomLeftRadius: 10,
        backgroundColor: '#FFFFFF',
    },
    textTitleStyle: {
        paddingTop: 10,
    },
    paddingLeft: {
        paddingLeft: 7.5,
    },
    textContainer: {
        marginTop: 5,
        flexDirection: 'row',
        flex: 1,
        paddingTop: 10,
    },
    textSeperate: {
        color: '#AAAAAA',
        paddingRight: 10,
    },
    likeContainerStyle: {
        position: 'absolute',
        right: 5,
        top: 25,
        flexDirection: 'row',
    },
    fontSize: {
        fontSize: 18,
    },
    loveLogo: {
        paddingTop: 2.5,
    }
});