import React, { useState } from 'react';
import { Image, ImageBackground, Text, TouchableOpacity, View } from 'react-native';
import { numbToMoney } from '../../helper';
import { FavoritIcons, LoveIcons } from '../../icons';
import ForkIcons from '../../icons/ForkIcons';
import StarIcons from '../../icons/StarIcons';
import VehicleIcons from '../../icons/VehicleIcons';
import { IMenuCard } from './interface';
import { styles } from './MenuCard.Styles';

const MenuCard = (props: IMenuCard) => {
    const [isLiked, setIsLiked] = useState(props.data.isFav)

    const changeLike = () => setIsLiked(!isLiked);

    return (
        <>
            <ImageBackground
                source={props.data.store_image !== ""
                    ? { uri: props.data.store_image }
                    : require('../../images/noFoodPic.png')}
                style={styles.container}>
                <View style={[styles.upperContainer]}>
                    <Image
                        source={props.data.store_logo !== ""
                            ? { uri: props.data.store_logo }
                            : require('../../images/noFoodPicBowl.png')}
                        style={styles.pictStyle} />
                </View>
            </ImageBackground>
            <View style={styles.containerBottom}>
                <Text style={[styles.textTitleStyle, styles.paddingLeft, styles.fontSize]}>
                    Bale Bakaran Godean
                </Text>
                <View style={[styles.textContainer, styles.paddingLeft]}>
                    <View style={{ paddingTop: 3.5, paddingRight: 4 }}>
                        <ForkIcons />
                    </View>
                    <Text style={styles.textSeperate}>
                        5
                    </Text>
                    <View style={{ paddingTop: 3.5, paddingRight: 1 }}>
                        <StarIcons />
                    </View>
                    <Text style={{ color: '#FFBB00', paddingRight: 2 }}>
                        5.0
                    </Text>
                    <Text style={styles.textSeperate}>
                        ({props.data.num_reviews})
                    </Text>
                    <View style={{ paddingTop: 3.5, paddingRight: 4 }}>
                        <VehicleIcons />
                    </View>
                    <Text style={styles.textSeperate}>
                        {`Rp${numbToMoney(props.data.delivery_price.toString())}`}
                    </Text>
                </View>
                <TouchableOpacity style={styles.likeContainerStyle}>
                    <View style={styles.loveLogo}>
                        {isLiked
                            ? <LoveIcons color="#FF4A4A" />
                            : <FavoritIcons color="#AAAAAA" />
                        }
                    </View>
                    <Text style={[{ color: isLiked ? `#FF4A4A` : `#AAAAAA`, paddingLeft: 5 }, styles.fontSize]}>
                        {props.data.num_offers}
                    </Text>
                </TouchableOpacity>
            </View>
        </>
    );
};

export default MenuCard;
