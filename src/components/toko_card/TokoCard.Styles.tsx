import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        height: 90,
        width: 120,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
    },
    bottomContainer: {
        backgroundColor: '#00A3E0',
        height: 30,
        width: 120,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
    },
    textWarp: {
        color: 'white',
        flex: 1,
        flexWrap: 'wrap',
        textAlign: 'center',
        textAlignVertical: 'center',
        padding: 2.5,
    },
    placeholderPict: {
        height: 40,
        width: 40,
        backgroundColor: 'grey',
        borderRadius: 20,
    },
    pictPosition: {
        marginTop: 5,
        marginBottom: 1,
        alignSelf: 'center',
    },
    tokoTitleStyle: {
        paddingVertical: 2.5,
        flex: 1,
        flexWrap: 'wrap',
        textAlign: 'center',
        textAlignVertical: 'center',
    }
});