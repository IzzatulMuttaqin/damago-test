import React from 'react';
import { Image, Text, View } from 'react-native';
import { ITokoCardProps } from './interface';
import { styles } from './TokoCard.Styles';

const TokoCard = (props: ITokoCardProps) => {
    return (
        <>
            <View>
                <View style={styles.container}>
                    {props.toko_image !== ""
                        ? <Image source={{ uri: props.toko_image }} style={[styles.placeholderPict, styles.pictPosition]} />
                        : <Image source={require('../../images/noFoodPicBowl.png')} style={[styles.placeholderPict, styles.pictPosition]} />
                    }
                    <Text style={styles.tokoTitleStyle}>
                        {props.toko_name}
                    </Text>
                </View>
                <View style={styles.bottomContainer}>
                    <Text style={styles.textWarp}>{props.toko_total} Toko</Text>
                </View>
            </View>
        </>
    );
};

export default TokoCard;
