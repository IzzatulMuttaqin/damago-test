export interface ITokoCardProps {
    toko_image: string;
    toko_total: string;
    toko_name: string;
}