import React from 'react';
import Svg, { Line } from 'react-native-svg';

const CancelIcons = (props: any) => {
    return (
        <Svg width="25" height="25" viewBox="0 0 25 25" fill="none">
            <Line x1="5.657" y1="5.65663" x2="16.9707" y2="16.9703" stroke="#AAAAAA" stroke-width="12" />
            <Line x1="5.65697" y1="16.9703" x2="16.9707" y2="5.65658" stroke="#AAAAAA" stroke-width="12" />
        </Svg>
    )
}

export default CancelIcons;