import React from 'react';
import Svg, { Path } from 'react-native-svg';

const DownArrowIcons = (props: any) => {
    return (
        <Svg width="12" height="8" viewBox="0 0 12 8" fill="none">
            <Path d="M6.53033 7.03033C6.23744 7.32322 5.76256 7.32322 5.46967 7.03033L0.696699 2.25736C0.403806 1.96447 0.403806 1.48959 0.696699 1.1967C0.989593 0.903806 1.46447 0.903806 1.75736 1.1967L6 5.43934L10.2426 1.1967C10.5355 0.903806 11.0104 0.903806 11.3033 1.1967C11.5962 1.48959 11.5962 1.96447 11.3033 2.25736L6.53033 7.03033ZM6.75 6V6.5H5.25V6H6.75Z" fill="white" />
        </Svg>

    )
}

export default DownArrowIcons;