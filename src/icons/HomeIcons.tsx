import React from 'react';
import Svg, { Path } from 'react-native-svg';

const HomeIcons = (props: any) => {
    return (
        <Svg width="24" height="20" viewBox="0 0 24 20" fill="none">
            <Path d="M9.36654 19.9976H3.52533V10.5784H0L11.7351 0L23.4702 10.5784H19.9449V20H14.1037V12.947H9.36654V19.9976Z" fill="white" />
        </Svg>
    )
}

export default HomeIcons;