import React from 'react';
import Svg, { Path } from 'react-native-svg';

const LocationIcons = (props: any) => {
    return (
        <Svg width="12" height="16" viewBox="0 0 12 16" fill="none">
            <Path d="M5.81818 0C2.61818 0 0 2.61818 0 5.81818C0 9.01818 5.81818 16 5.81818 16C5.81818 16 11.6364 9.01818 11.6364 5.81818C11.6364 2.61818 9.01818 0 5.81818 0ZM5.81818 3.63636C6.98182 3.63636 8 4.65455 8 5.81818C8 6.98182 6.98182 8 5.81818 8C4.65455 8 3.63636 6.98182 3.63636 5.81818C3.63636 4.65455 4.65455 3.63636 5.81818 3.63636Z" fill="white" />
        </Svg>
    )
}

export default LocationIcons;