import React from 'react';
import Svg, { Path } from 'react-native-svg';

const StarIcons = (props: any) => {
    return (
        <Svg width="15" height="14" viewBox="0 0 15 14" fill="none">
            <Path d="M6.5 0L8.60133 3.60776L12.6819 4.49139L9.90003 7.60474L10.3206 11.7586L6.5 10.075L2.6794 11.7586L3.09997 7.60474L0.318133 4.49139L4.39867 3.60776L6.5 0Z" fill="#FFBB00" />
        </Svg>
    )
}

export default StarIcons;