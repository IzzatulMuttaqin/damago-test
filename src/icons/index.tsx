import JelajahiIcons from './JelajahiIcons';
import PetaTokoIcons from './PetaTokoIcons';
import FavoritIcons from './FavoritIcons';
import PrsananIcons from './PesananIcons';
import RiwayatIcons from './RiwayatIcons';
import LoveIcons from './LoveIcons';
import ChartIcons from './ChartIcons';
import CancelIcons from './CancelIcons';
import SearchIcons from './SearchIcons';

export {
    JelajahiIcons, 
    PetaTokoIcons,
    FavoritIcons, 
    PrsananIcons,
    RiwayatIcons, 
    LoveIcons,
    ChartIcons,
    CancelIcons,
    SearchIcons,
};