import React from 'react';
import { SafeAreaView, View, StatusBar } from 'react-native';

const BlankView = () => {
    return (
        <>
            <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
            <SafeAreaView>
                <View />
            </SafeAreaView>
        </>
    );
};

export default BlankView;
