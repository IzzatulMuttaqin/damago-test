import Axios from 'axios';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Platform,
  ImageBackground,
  TextInput,
} from 'react-native';
import { Categories, Chart, MenuCard, TokoCard } from '../../components';
import { SearchIcons } from '../../icons';
import DownArrowIcons from '../../icons/DownArrowIcons';
import HomeIcons from '../../icons/HomeIcons';
import LocationIcons from '../../icons/LocationIcons';
import { IStore } from './interface';

const Home = () => {
  const [data, setData] = useState<IStore[]>([]);
  const [justJoined, setJustJoined] = useState<IStore[]>([]);
  const [favorites, setFavorites] = useState<IStore[]>([]);
  const [dontMiss, setDontMiss] = useState<IStore[]>([]);
  const [init, setInit] = useState<boolean>(true);
  const [showMore, setShowMore] = useState(false);

  const getData = async () => {
    await Axios.get<IStore[]>(`http://localhost:5000/stores`)
      .then(success => {
        const justJoin = _.cloneDeep(success.data).sort((a, b) => (a.id > b.id ? -1 : 1));
        const favorite = _.cloneDeep(success.data).sort((a, b) => (a.num_offers > b.num_offers ? -1 : 1))
        const dontMiss = _.cloneDeep(success.data)
          .sort((a, b) => (a.num_offers < b.num_offers ? -1 : 1))
          .filter((item) => item.num_offers > 0);
        setDontMiss(dontMiss);
        setJustJoined(justJoin);
        setFavorites(favorite);
        setData(success.data);
      })
      .catch(error => {
        console.log(error.message);
      });
  }

  useEffect(() => {
    const getDataCaller = async () => {
      await getData();
    }

    if (init) {
      getDataCaller();
      setInit(false);
    }
  }, []);

  const showMoreToggle = () => { setShowMore(!showMore) };

  let paddingTop = 34;
  if (Platform.OS === 'android') {
    const hasNotch = StatusBar.currentHeight;
    if (hasNotch && hasNotch <= 24) paddingTop = 20;
  }

  return (
    <>
      <StatusBar translucent backgroundColor='#E5E5E5' barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          style={[styles.scrollView, { paddingTop: paddingTop }]}
          showsVerticalScrollIndicator={false}
        >
          <View>
            <ImageBackground style={styles.banner} source={require('../../images/banner.png')} />
            <View style={{ height: 20 }} />
            <View style={{ flexDirection: 'row', padding: 15 }}>
              <HomeIcons />
              <View style={{ paddingTop: 2.5, paddingLeft: 60, paddingRight: 5 }}>
                <LocationIcons />
              </View>
              <Text style={{ fontSize: 13, color: 'white', paddingRight: 5 }}>
                Kec. Martoyudan, Magelang
              </Text>
              <View style={{ paddingTop: 5 }}>
                <DownArrowIcons />
              </View>
            </View>
            <View>
              <View style={{ width: 380, padding: 10, height: 35, backgroundColor: 'white', marginHorizontal: 15, borderRadius: 10, flexDirection: 'row' }}>
                <SearchIcons />
              </View>
              <TextInput style={{ position: "absolute", left: 50, top: -6.25, width: 340 }} placeholder="Hari ini mau makan apa?" />
            </View>
            <View style={{ height: 180 }} />
            <View style={styles.categories}>
              <Categories state={showMore} onChange={showMoreToggle} />
            </View>
          </View>

          <View style={{ height: 17.5 }} />

          <View>
            <Text style={styles.subTitle}>
              Cabang Toko
            </Text>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={
                ({ item, index }) =>
                  <View style={index === 0
                    ? [styles.tokoList, { paddingLeft: 15 }]
                    : index === data.length - 1
                      ? [styles.tokoList, { paddingRight: 15 }] :
                      styles.tokoList}>
                    <TokoCard toko_image={item.store_logo} toko_name={item.store_name} toko_total="10" />
                  </View>
              }
              data={data}
              keyExtractor={(item: IStore) => `${item.id}`}
            />
          </View>

          <View style={{ height: 17.5 }} />

          <View>
            <Text style={styles.subTitle}>
              Baru aja bergabung nih!
            </Text>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={
                ({ item, index }) =>
                  <View style={index === 0
                    ? [styles.tokoList, { paddingLeft: 15 }]
                    : index === justJoined.length - 1
                      ? [styles.tokoList, { paddingRight: 15 }] :
                      styles.tokoList}>
                    <MenuCard data={item} />
                  </View>
              }
              data={justJoined}
              keyExtractor={(item: IStore) => `${item.id}`}
            />
          </View>

          <View style={{ height: 17.5 }} />

          <View>
            <Text style={styles.subTitle}>
              Favorites!
            </Text>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={
                ({ item, index }) =>
                  <View style={index === 0
                    ? [styles.tokoList, { paddingLeft: 15 }]
                    : index === favorites.length - 1
                      ? [styles.tokoList, { paddingRight: 15 }] :
                      styles.tokoList}>
                    <MenuCard data={item} />
                  </View>
              }
              data={favorites}
              keyExtractor={(item: IStore) => `${item.id}`}
            />
          </View>

          <View style={{ height: 17.5 }} />

          <View>
            <Text style={styles.subTitle}>
              Dont miss out
            </Text>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              renderItem={
                ({ item, index }) =>
                  <View style={index === 0
                    ? [styles.tokoList, { paddingLeft: 15 }]
                    : index === dontMiss.length - 1
                      ? [styles.tokoList, { paddingRight: 15 }] :
                      styles.tokoList}>
                    <MenuCard data={item} />
                  </View>
              }
              data={dontMiss}
              keyExtractor={(item: IStore) => `${item.id}`}
            />
          </View>

          <View style={{ height: 50 }} />

        </ScrollView>
        <Chart />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#E5E5E5',
  },
  tokoList: {
    paddingHorizontal: 5,
  },
  subTitle: {
    fontWeight: 'bold',
    fontSize: 22,
    paddingLeft: 15,
    paddingBottom: 15,
  },
  categories: {
    alignContent: 'center',
    alignItems: 'center',
  },
  banner: {
    width: '100%',
    height: 300,
    position: 'absolute',
  }
});

export default Home;
